# Hexa dokuwiki template

* Based on a wordpress theme
* Designed by [Automattic](https://wordpress.org/themes/hexa/)
* Converted by [desbest](http://desbest.com)
* Metadata is in template.info.txt
* Under the GPL license (see copying file)
* [More information](http://dokuwiki.org/template:hexa)

![hexa theme screenshot](https://i.imgur.com/jyfHJwn.png)