<?php
/**
 * DokuWiki Hexa Template
 * Based on the starter template and a wordpress theme of the same name
 *
 * @link     http://dokuwiki.org/template:hexa
 * @author   desbest <afaninthehouse@gmail.com>
 * @license  GPL 2 (http://www.gnu.org/licenses/gpl.html)
 */

if (!defined('DOKU_INC')) die(); /* must be run from within DokuWiki */
@require_once(dirname(__FILE__).'/tpl_functions.php'); /* include hook for template functions */
header('X-UA-Compatible: IE=edge,chrome=1');

$showTools = !tpl_getConf('hideTools') || ( tpl_getConf('hideTools') && !empty($_SERVER['REMOTE_USER']) );
$showSidebar = page_findnearest($conf['sidebar']) && ($ACT=='show');
?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $conf['lang'] ?>"
  lang="<?php echo $conf['lang'] ?>" dir="<?php echo $lang['direction'] ?>" class="no-js">
<head>
    <meta charset="UTF-8" />
    <link rel='dns-prefetch' href='//fonts.googleapis.com' />
    <title><?php tpl_pagetitle() ?> [<?php echo strip_tags($conf['title']) ?>]</title>
    <script>(function(H){H.className=H.className.replace(/\bno-js\b/,'js')})(document.documentElement)</script>
    <?php tpl_metaheaders() ?>
    <meta name="viewport" content="width=device-width,initial-scale=1" />
    <?php echo tpl_favicon(array('favicon', 'mobile')) ?>
    <?php tpl_includeFile('meta.html') ?>
    <link rel='stylesheet' id='hexa-source-sans-pro-css'  href='https://fonts.googleapis.com/css?family=Source+Sans+Pro%3A300%2C400%2C700%2C300italic%2C400italic%2C700italic&#038;ver=5.4.4' type='text/css' media='all' />
</head>

<body id="dokuwiki__top" class="site <?php echo tpl_classes(); ?> <?php  echo ($showSidebar) ? 'hasSidebar' : ''; ?>">

 <ul class="a11y skip">
<li><a href="#dokuwiki__content"><?php echo $lang['skip_to_content'] ?></a></li>
</ul>


<div id="page" class="hfeed site">
    <?php //do_action( 'before' ); ?>
    <?php /* how to insert logo instead (if no CSS image replacement technique is used):
        upload your logo into the data/media folder (root of the media manager) and replace 'logo.png' accordingly:
        tpl_link(wl(),'<img src="'.ml('logo.png').'" alt="'.$conf['title'].'" />','id="dokuwiki__top" accesskey="h" title="[H]"') */ ?>
    <div id="menu-toggle-nav" class="panel">
        <nav id="site-navigation" class="main-navigation" role="navigation">
            <a class="skip-link screen-reader-text" href="#content">Skip to content</a>

            <?php //wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
            <div class="menu-my-first-menu-container"><ul id="menu-my-first-menu" class="menu">
             <!-- SITE TOOLS -->
            <h3 class="a11y"><?php echo $lang['site_tools'] ?></h3>
        
            <?php tpl_toolsevent('sitetools', array(
                'recent'    => tpl_action('recent', 1, 'li', 1),
                'media'     => tpl_action('media', 1, 'li', 1),
                'index'     => tpl_action('index', 1, 'li', 1),
            )); ?>

            </ul></div>
        </nav><!-- #site-navigation -->
    </div>

    <div id="sidebar-toggle-nav" class="panel">
    <div class="widget-areas">
    <div class="widget-area">

    <aside id="pagetools" class="widget"><h1 class="widget-title">Page Tools</h1>     <ul>
      <!-- PAGE ACTIONS -->
        <?php if ($showTools): ?>
        <h3 class="a11y"><?php echo $lang['page_tools'] ?></h3>
            <?php tpl_toolsevent('pagetools', array(
                'edit'      => tpl_action('edit', 1, 'li', 1),
                'discussion'=> _tpl_action('discussion', 1, 'li', 1),
                'revisions' => tpl_action('revisions', 1, 'li', 1),
                'backlink'  => tpl_action('backlink', 1, 'li', 1),
                'subscribe' => tpl_action('subscribe', 1, 'li', 1),
                'revert'    => tpl_action('revert', 1, 'li', 1),
                'top'       => tpl_action('top', 1, 'li', 1),
            )); ?>
    </li>
    </ul>
    </aside>        

    <aside id="usertools" class="widget"><h1 class="widget-title">User Tools</h1>      <ul>
   
        <?php endif; ?>
         <!-- USER TOOLS -->
        <?php if ($conf['useacl'] && $showTools): ?>
        <h3 class="a11y"><?php echo $lang['user_tools'] ?></h3>
            <?php
                if (!empty($_SERVER['REMOTE_USER'])) {
                    echo '<li class="user">';
                    tpl_userinfo(); /* 'Logged in as ...' */
                    echo '</li>';
                }
            ?>
            <?php /* the optional second parameter of tpl_action() switches between a link and a button,
                     e.g. a button inside a <li> would be: tpl_action('edit', 0, 'li') */
            ?>
            <?php tpl_toolsevent('usertools', array(
                'admin'     => tpl_action('admin', 1, 'li', 1),
                'userpage'  => _tpl_action('userpage', 1, 'li', 1),
                'profile'   => tpl_action('profile', 1, 'li', 1),
                'register'  => tpl_action('register', 1, 'li', 1),
                'login'     => tpl_action('login', 1, 'li', 1),
            )); ?>
        <?php endif ?>

    </ul>
    </aside>

    <aside id="writtensidebar">
        <!-- ********** ASIDE ********** -->
        <?php if ($showSidebar): ?>
        <?php tpl_includeFile('sidebarheader.html') ?>
        <?php tpl_include_page($conf['sidebar'], 1, 1) /* includes the nearest sidebar page */ ?>
        <?php tpl_includeFile('sidebarfooter.html') ?>
        <div class="clearer"></div>
        <?php endif; ?>
    </aside>

    </div>
    </div>
    </div>
    <?php //if ( is_active_sidebar( 'sidebar-1' ) || is_active_sidebar( 'sidebar-2' ) || is_active_sidebar( 'sidebar-3' ) ) : ?>
        <?php //get_sidebar(); ?>
    <?php //endif; ?>
        <div id="social-links-toggle-nav" class="panel">
            <?php //wp_nav_menu( array( 'theme_location' => 'social', 'depth' => 1, 'link_before' => '<span class="screen-reader-text">', 'link_after' => '</span>', 'container_class' => 'social-links', ) ); ?>
        </div>
    <div id="search-toggle-nav" class="panel">
        <div class="search-wrapper">
            <?php //get_search_form(); ?>
            <?php tpl_searchform() ?>
            <!-- <form role="search" method="get" class="search-form" action="http://localhost/wordpress/">
            <label>
            <span class="screen-reader-text">Search for:</span>
            <input type="search" class="search-field" placeholder="Search …" value="" name="s">
            </label>
            <input type="submit" class="search-submit" value="Search">
            </form> -->
        </div>
    </div>
    <?php tpl_includeFile('header.html') ?>
    <header id="masthead" class="site-header" role="banner">
        <div class="site-header-wrapper">
            <div class="site-branding">
                <h1 class="site-title"><?php tpl_link(wl(),$conf['title'],'accesskey="h" title="[H]"') ?></h1>
                <?php if ($conf['tagline']): ?>
                    <h2 class="site-description"><?php echo $conf['tagline'] ?></h2>
                <?php endif ?>
            </div>
            <div class="toggles">
            <div id="menu-toggle" class="toggle" title="Menu">
                <span class="screen-reader-text">Menu</span>
            </div><!-- #menu-toggle -->
            <?php //if ( is_active_sidebar( 'sidebar-1' ) || is_active_sidebar( 'sidebar-2' ) || is_active_sidebar( 'sidebar-3' ) ) : ?>
                <div id="sidebar-toggle" class="toggle" title="Widgets">
                    <span class="screen-reader-text">Widgets</span>
                </div><!-- #sidebar-toggle -->
            <?php //endif; ?>
            <?php //if ( has_nav_menu( 'social' ) ) : ?>
               <!--  <div id="social-links-toggle" class="toggle" title="Social Links">
                    <span class="screen-reader-text">Social Links</span>
                </div> --><!-- #social-links-toggle -->
            <?php //endif; ?>
            <div id="search-toggle" class="toggle" title="Search">
                <span class="screen-reader-text">Search</span>
            </div><!-- #search-toggle -->
        </div><!-- .toggles -->
        </div>
    </header><!-- #masthead -->

    <div id="content" class="site-content">
    <div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">

        <?php //get_template_part( 'content', 'single' ); ?>
        <article class="hentry" <?php //post_class(); ?>>

            <header class="entry-header">
            <!-- <a class="entry-format" href="" title=""><span class="screen-reader-text">stuff here</span></a> -->
            <span class="entry-format"></span>
            <!-- <h1 class="entry-title">&nbsp;</h1> -->
            </header>
            <!-- .entry-header -->

            <div class="entry-content">

            <!-- BREADCRUMBS -->
            <?php if($conf['breadcrumbs']){ ?>
                <div class="breadcrumbs"><?php tpl_breadcrumbs() ?></div>
            <?php } ?>
            <?php if($conf['youarehere']){ ?>
                <div class="breadcrumbs"><?php tpl_youarehere() ?></div>
            <?php } ?>

            <?php //the_content(); ?>
            <!-- ********** CONTENT ********** -->
            <div id="dokuwiki__content">
                <?php tpl_flush() /* flush the output buffer */ ?>
                <?php tpl_includeFile('pageheader.html') ?>

                <div class="page">
                    <!-- wikipage start -->
                    <?php tpl_content() /* the main content */ ?>
                    <!-- wikipage stop -->
                    <div class="clearer"></div>
                </div>

                <?php tpl_flush() ?>
                <?php tpl_includeFile('pagefooter.html') ?>
            </div><!-- /content -->
            <?php html_msgarea() /* occasional error and info messages on top of the page */ ?>
            <?php //wp_link_pages( array( 'before' => '<div class="page-links">', 'after' => '</div>', 'link_before' => '<span class="active-link">', 'link_after' => '</span>' ) ); ?>
            </div><!-- .entry-content -->

            <footer class="entry-meta">
            <?php //hexa_posted_on(); ?>
            <?php
            /* translators: used between list items, there is a space after the comma */
            //the_tags( '<span class="tags-links">', esc_html__( ', ', 'hexa' ), '</span>' );
            ?>
            <?php //edit_post_link( __( 'Edit', 'hexa' ), '<span class="edit-link">', '</span>' ); ?>
            </footer><!-- .entry-meta -->
        </article><!-- #post-## -->

        <?php //hexa_post_nav(); ?>

        <?php
        // If comments are open or we have at least one comment, load up the comment template
        // if ( comments_open() || '0' != get_comments_number() ) :
        // comments_template();
        // endif;
        ?>

    </main><!-- #main -->
    </div><!-- #primary -->
    </div><!-- #content -->

    <footer id="colophon" class="site-footer" role="contentinfo">
        <div class="site-info">
            <div class="doc"><?php tpl_pageinfo() /* 'Last modified' etc */ ?></div>
            <?php tpl_license('button') /* content license, parameters: img=*badge|button|0, imgonly=*0|1, return=*0|1 */ ?>
            <a href="http://dokuwiki.org/" rel="generator">Powered by Dokuwiki</a>
            <span class="sep"> | </span>
            Hexa theme by <a href="http://wordpress.com/themes/hexa/" rel="designer">Automattic</a> and <a href="http://desbest.com">desbest</a></a>
            <div class="no"><?php tpl_indexerWebBug() /* provide DokuWiki housekeeping, required in all templates */ ?></div>
        </div><!-- .site-info -->
    </footer><!-- #colophon -->
    <?php tpl_includeFile('footer.html') ?>
</div><!-- #page -->


    <?php /* the "dokuwiki__top" id is needed somewhere at the top, because that's where the "back to top" button/link links to */ ?>
    <?php /* tpl_classes() provides useful CSS classes; if you choose not to use it, the 'dokuwiki' class at least
             should always be in one of the surrounding elements (e.g. plugins and templates depend on it) */ ?>

               <!-- this has to be at the bottom of the 
        page due to the way dokuwiki renders javascript -->
    <script defer type="text/javascript" src="<?php echo tpl_basedir();?>/menus.js"></script>
    <script defer type="text/javascript" src="<?php echo tpl_basedir();?>/mobileinputsize.js"></script>
    <script defer type="text/javascript" src="<?php echo tpl_basedir();?>/navigation.js"></script>
    <!-- due to the way dokuwiki buffers output, this javascript has to
            be before the </body> tag and not in the <head> -->
</body>
</html>
